#@field PREFIX
#@type STRING
#Prefix for EPICS PVs.
#
#@field IPADDR
#@type STRING
#IP address of the Moxa.
#
#@field IPPORT
#@type STRING
#TCP port number on the Moxa.

#Specify the TCP endpoint and give your 'bus' an arbitrary name eg. "stream".
drvAsynIPPortConfigure("$(PREFIX)stream", "$(IPADDR):$(IPPORT)")
asynInterposeEcho("$(PREFIX)stream")

#asynSetTraceMask("$(PREFIX)stream", -1, 0x9)
#asynSetTraceIOMask("$(PREFIX)stream", -1, 0x2)

#Load your database defining the EPICS records
dbLoadRecords("nhq203m.db", "PREFIX=$(PREFIX), PROTOCOL=nhq203m.proto, PORT=$(PREFIX)stream")
