from org.csstudio.display.builder.runtime.script import PVUtil

mystr = PVUtil.getString(pvs[0])
if mystr == 'ON ':
   mytext = 'Output voltage according to set voltage'   
elif mystr == 'OFF':
   mytext = "Channel front panel switched OFF"  
elif mystr == 'MAN':
   mytext = "Channel is on, set to MANUAL mode"  
elif mystr == 'ERR':
   mytext = "Vmax or Imax is/was exceeded"  
elif mystr == 'INH':
   mytext = "Inhibit signal is/was active"  
elif mystr == 'QUA':
   mytext = "Quality of output voltage not given at present"  
elif mystr == 'L2H':
   mytext = "Output voltage increasing"  
elif mystr == 'H2L':
   mytext = "Output voltage falling"  
elif mystr == 'LAS':
   mytext = "Look at Status"  
elif mystr == 'TRP':
   mytext = "Current trip was active"
else:
    mytext = "Unrecognized...."

pvs[1].setValue(mytext)
